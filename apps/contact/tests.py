from django.test import TestCase
from django.urls import reverse

from .models import Contact

def create_contact(firstname, lastname, phone, email):
    return Contact.objects.create(firstname=firstname, lastname=lastname, phone=phone, email=email)

class IndexViewTest(TestCase):
    def no_contacts(self):
        response = self.client.get(reverse('contact:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Aucun contact.')
        self.assertQuerysetEqual(response.context['contact_list'], [])