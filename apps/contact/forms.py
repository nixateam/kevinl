from django.forms import ModelForm

from .models import Address, Contact

class ContactForm(ModelForm):
    class Meta:
        model = Contact
        exclude = ['user']

class AddressForm(ModelForm):
    class Meta:
        model = Address
        exclude = ['contact']

