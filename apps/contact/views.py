from django.views import generic
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.shortcuts import render
from django.template.loader import render_to_string
from django.db.models import Q
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied

from .models import Contact
from .forms import AddressForm, ContactForm

class IndexView(LoginRequiredMixin, generic.ListView):
    template_name = 'contact/index.html'
    context_object_name = 'contact_list'

    def get_queryset(self):
        user = self.request.user
        return Contact.objects.filter(user=user).order_by('firstname')

class DetailView(LoginRequiredMixin, generic.DetailView):
    model = Contact
    context_object_name = 'contact'
    template_name = 'contact/detail.html'

    def dispatch(self, request, *args, **kwargs):
        user = request.user
        contact = self.get_object()
        if user != contact.user:
            raise PermissionDenied
        return super(DetailView, self).dispatch(request, *args, **kwargs)

    def get_object(self):
        return super().get_object()

class AddView(LoginRequiredMixin, generic.View):

    form = ContactForm
    address_form = AddressForm

    template = 'contact/add.html'

    def get(self, request):
        return render(request, self.template, {
            'form': self.form(),
            'address_form': self.address_form()
        })

    def post(self, request):
        form = self.form(request.POST)
        address_form = self.address_form(request.POST)
        if form.is_valid() and address_form.is_valid():
            contact = form.save(commit=False)
            contact.user = request.user
            contact.save()
            address = address_form.save(commit=False)
            address.contact = contact
            address.save()
            return HttpResponseRedirect(reverse('contact:index'))

        return render(request, self.template, {
            'form': form(),
            'address_form': address_form()
        })

class SearchView(generic.View):
    def get(self, request):
        text = request.GET.get('text')
        contacts = Contact.objects.filter(Q(firstname__icontains=text) | Q(lastname__icontains=text)).order_by('firstname')
        rendered = render_to_string('contact/contact-list.html', {'contact_list': contacts})
        return JsonResponse({'data': rendered})