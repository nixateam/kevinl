from django.db import models
from django.conf import settings

class Contact(models.Model):

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    firstname = models.CharField(max_length=255)
    lastname = models.CharField(max_length=255)
    email = models.CharField(max_length=255, unique=True)
    phone = models.CharField(max_length=255)
    creation_date = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        self.phone = "%s%s%s %s%s%s-%s%s%s%s" % tuple(self.phone)
        super(Contact, self).save(*args, **kwargs)

    def __str__(self):
        return self.firstname + ' ' + self.lastname

class Address(models.Model):

    AB, BC, MB, NB, NL, NS, ON, PE, QC, SK = range(10)
    PROVINCES = (
        (AB, 'Alberta'),
        (BC, 'British Columbia'),
        (MB, 'Manitoba'),
        (NB, 'New Brunswick'),
        (NL, 'Newfoundland and Labrador'),
        (NS, 'Nova Scotia'),
        (ON, 'Ontario'),
        (PE, 'Prince Edward Island'),
        (QC, 'Quebec'),
        (SK, 'Saskatchewan'),
    )

    contact = models.OneToOneField(Contact, on_delete=models.CASCADE)
    street_name = models.CharField(max_length=255)
    street_number = models.IntegerField()
    city = models.CharField(max_length=255)
    zip_code = models.CharField(max_length=10)
    province = models.IntegerField(choices=PROVINCES)
