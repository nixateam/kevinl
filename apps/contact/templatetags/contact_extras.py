from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()

@register.filter
@stringfilter
def ucfirst(value):
    return value[0].upper() + value[1:]