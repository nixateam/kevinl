import Contact from './contact.es6'

$(() => {
    let contact = Contact;
    $('#search-input').on('keyup', () => {
        contact.init();
    });
});