class Contact {
    init() {
        let $form = $('#search-contact-form');
        let self = this;

        $.get($form.attr('action'), $form.serialize(), self.postSuccess)
            .fail(self.postError);
    }

    postSuccess(response) {
        $('#contact-list').html(response.data)

    }

    postError(response) {
        console.log(response)
    }
}

export default new Contact();