from django.conf.urls import include, url

from . import views

app_name = 'contact'
urlpatterns = [
    url(r'^add/$', views.AddView.as_view(), name='add_contact'),
    url(r'^search/$', views.SearchView.as_view(), name='search'),
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    url(r'^', views.IndexView.as_view(), name='index'),
]