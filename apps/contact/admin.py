from django.contrib import admin

from .models import Address, Contact

class AddressInline(admin.StackedInline):
    model = Address

class ContactAdmin(admin.ModelAdmin):
    list_display = ('firstname', 'lastname', 'phone', 'email', 'creation_date')
    inlines = [AddressInline]

admin.site.register(Contact, ContactAdmin)
